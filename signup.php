<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Web-0-GIS </title>

  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link href="https://fonts.googleapis.com/css?family=Allura|Julius+Sans+One|Maitree|Quicksand" rel="stylesheet"> 
  <link type="text/css" rel="stylesheet" href="styles/styleSignUp.css" >
  <script src="scripts/signup.js" ></script>
</head>
<body>
  <?php
  session_start();
  $_SESSION['message']='';
  $mysqli = new mysqli('localhost','root','','web-0-gis');
  if ($_SERVER['REQUEST_METHOD']== 'POST')
  {
    if($_POST['password'] == $_POST['rePassword'])//checking if both the p/w are same
    {
      $firstName = $mysqli-> real_escape_string($_POST['firstName']);
      $lastName = $mysqli-> real_escape_string($_POST['lastName']);
      $userName = $mysqli-> real_escape_string($_POST['userName']);
      $email = $mysqli-> real_escape_string($_POST['email']);
      $password = md5($_POST['password']); 

      $_SESSION['userName'] = $userName;
      $sql = " INSERT INTO users (firstName, lastName, userName, email, password) VALUES ('$firstName','$lastName','$userName','$email','$password')";
      if($mysqli->query($sql) === true)
      {
        $_SESSION['message'] = 'Registration Successful !!';
        header("location: login.html");
      }
      else
      {
        $_SESSION['message'] = 'Sorry, the user could not be registered :(';
      }
    }
    else{
      $_SESSION['message'] = 'Passwords donot match';
      }
    }
  ?>
  <center>
  <div id="formbox">
    <h2 id="sutitle"><img id="logo" src="images/favicon.ico">Sign Up</h2>
    <form name="signUp" method="POST" enctype="multipart/form-data" autocomplete="off">
    <div class="alert alert-error">
      <?= $_SESSION['message']?>
    </div>
      First Name:<br> <input type="text" name="firstName" placeholder="john" maxlength="20" id="firstName"><br>
      Last Name:<br><input type="text" name="lastName" placeholder="Smith" maxlength="20" id="lastName" ><br>
      User Name:<br> <input type="text" name="userName"
      placeholder="JSmith27" id="userName"><br>
      Email:<br><input type="email" name="email" placeholder="jsmith@gmail.com" id ="email" ><br>
      Password:<br> <input type="password" name="password" placeholder="**********" id="password"> <br>
      Repeat Password:<br> <input type="password" name="rePassword" placeholder="**********" id="rePassword"> <br>
      <br>
      <button id="cancel"type="reset">Cancel</button>
      <button id="submit" type="submit" >Sign up</button>
      </form>
  </div>
</center>

</body>
</html>